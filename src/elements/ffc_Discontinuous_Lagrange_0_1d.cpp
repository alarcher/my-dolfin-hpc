// This code conforms with the UFC specification version 2.0.6
// and was automatically generated by FFC version 1.0.1+.
// 
// This code was generated with the following parameters:
// 
//   cache_dir:                      ''
//   convert_exceptions_to_warnings: False
//   cpp_optimize:                   False
//   cpp_optimize_flags:             '-O2'
//   epsilon:                        1e-14
//   error_control:                  False
//   form_postfix:                   True
//   format:                         'ufc'
//   log_level:                      20
//   log_prefix:                     ''
//   no-evaluate_basis:              False
//   no-evaluate_basis_derivatives:  False
//   optimize:                       False
//   output_dir:                     '.'
//   precision:                      15
//   quadrature_degree:              'auto'
//   quadrature_rule:                'auto'
//   representation:                 'auto'
//   split:                          True
//   swig_binary:                    'swig'
//   swig_path:                      ''

#include "ffc_Discontinuous_Lagrange_0_1d.h"

/// Constructor
ffc_discontinuous_lagrange_0_1d_finite_element_0::ffc_discontinuous_lagrange_0_1d_finite_element_0() : ufc::finite_element()
{
    // Do nothing
}

/// Destructor
ffc_discontinuous_lagrange_0_1d_finite_element_0::~ffc_discontinuous_lagrange_0_1d_finite_element_0()
{
    // Do nothing
}

/// Return a string identifying the finite element
const char* ffc_discontinuous_lagrange_0_1d_finite_element_0::signature() const
{
    return "FiniteElement('Discontinuous Lagrange', Cell('interval', Space(1)), 0, None)";
}

/// Return the cell shape
ufc::shape ffc_discontinuous_lagrange_0_1d_finite_element_0::cell_shape() const
{
    return ufc::interval;
}

/// Return the topological dimension of the cell shape
unsigned int ffc_discontinuous_lagrange_0_1d_finite_element_0::topological_dimension() const
{
    return 1;
}

/// Return the geometric dimension of the cell shape
unsigned int ffc_discontinuous_lagrange_0_1d_finite_element_0::geometric_dimension() const
{
    return 1;
}

/// Return the dimension of the finite element function space
unsigned int ffc_discontinuous_lagrange_0_1d_finite_element_0::space_dimension() const
{
    return 1;
}

/// Return the rank of the value space
unsigned int ffc_discontinuous_lagrange_0_1d_finite_element_0::value_rank() const
{
    return 0;
}

/// Return the dimension of the value space for axis i
unsigned int ffc_discontinuous_lagrange_0_1d_finite_element_0::value_dimension(unsigned int i) const
{
    return 1;
}

/// Evaluate basis function i at given point in cell
void ffc_discontinuous_lagrange_0_1d_finite_element_0::evaluate_basis(unsigned int i,
                                   double* values,
                                   const double* coordinates,
                                   const ufc::cell& c) const
{
    // Extract vertex coordinates
    
    // Compute Jacobian of affine map from reference cell
    
    // Compute determinant of Jacobian
    
    // Compute inverse of Jacobian
    
    // Get coordinates and map to the reference (FIAT) element
    
    // Reset values.
    *values = 0.0;
    
    // Array of basisvalues.
    double basisvalues[1] = {0.0};
    
    // Declare helper variables.
    
    // Compute basisvalues.
    basisvalues[0] = 1.0;
    for (unsigned int r = 0; r < 1; r++)
    {
      basisvalues[r] *= std::sqrt((0.5 + r));
    }// end loop over 'r'
    
    // Table(s) of coefficients.
    static const double coefficients0[1] = \
    {1.41421356237309};
    
    // Compute value(s).
    for (unsigned int r = 0; r < 1; r++)
    {
      *values += coefficients0[r]*basisvalues[r];
    }// end loop over 'r'
}

/// Evaluate all basis functions at given point in cell
void ffc_discontinuous_lagrange_0_1d_finite_element_0::evaluate_basis_all(double* values,
                                       const double* coordinates,
                                       const ufc::cell& c) const
{
    // Element is constant, calling evaluate_basis.
    evaluate_basis(0, values, coordinates, c);
}

/// Evaluate order n derivatives of basis function i at given point in cell
void ffc_discontinuous_lagrange_0_1d_finite_element_0::evaluate_basis_derivatives(unsigned int i,
                                               unsigned int n,
                                               double* values,
                                               const double* coordinates,
                                               const ufc::cell& c) const
{
    // Extract vertex coordinates
    const double * const * x = c.coordinates;
    
    // Compute Jacobian of affine map from reference cell
    const double J_00 = x[1][0] - x[0][0];
    
    // Compute determinant of Jacobian
    const double detJ =  J_00;
    
    // Compute inverse of Jacobian
    const double K_00 =  1.0 / detJ;
    
    // Get coordinates and map to the reference (FIAT) element
    
    // Compute number of derivatives.
    unsigned int num_derivatives = 1;
    for (unsigned int r = 0; r < n; r++)
    {
      num_derivatives *= 1;
    }// end loop over 'r'
    
    // Declare pointer to two dimensional array that holds combinations of derivatives and initialise
    unsigned int **combinations = new unsigned int *[num_derivatives];
    for (unsigned int row = 0; row < num_derivatives; row++)
    {
      combinations[row] = new unsigned int [n];
      for (unsigned int col = 0; col < n; col++)
        combinations[row][col] = 0;
    }
    
    // Generate combinations of derivatives
    for (unsigned int row = 1; row < num_derivatives; row++)
    {
      for (unsigned int num = 0; num < row; num++)
      {
        for (unsigned int col = n-1; col+1 > 0; col--)
        {
          if (combinations[row][col] + 1 > 0)
            combinations[row][col] = 0;
          else
          {
            combinations[row][col] += 1;
            break;
          }
        }
      }
    }
    
    // Compute inverse of Jacobian
    const double Jinv[1][1] =  {{K_00}};
    
    // Declare transformation matrix
    // Declare pointer to two dimensional array and initialise
    double **transform = new double *[num_derivatives];
    
    for (unsigned int j = 0; j < num_derivatives; j++)
    {
      transform[j] = new double [num_derivatives];
      for (unsigned int k = 0; k < num_derivatives; k++)
        transform[j][k] = 1;
    }
    
    // Construct transformation matrix
    for (unsigned int row = 0; row < num_derivatives; row++)
    {
      for (unsigned int col = 0; col < num_derivatives; col++)
      {
        for (unsigned int k = 0; k < n; k++)
          transform[row][col] *= Jinv[combinations[col][k]][combinations[row][k]];
      }
    }
    
    // Reset values. Assuming that values is always an array.
    for (unsigned int r = 0; r < num_derivatives; r++)
    {
      values[r] = 0.0;
    }// end loop over 'r'
    
    
    // Array of basisvalues.
    double basisvalues[1] = {0.0};
    
    // Declare helper variables.
    
    // Compute basisvalues.
    basisvalues[0] = 1.0;
    for (unsigned int r = 0; r < 1; r++)
    {
      basisvalues[r] *= std::sqrt((0.5 + r));
    }// end loop over 'r'
    
    // Table(s) of coefficients.
    static const double coefficients0[1] = \
    {1.41421356237309};
    
    // Tables of derivatives of the polynomial base (transpose).
    static const double dmats0[1][1] = \
    {{0.0}};
    
    // Compute reference derivatives.
    // Declare pointer to array of derivatives on FIAT element.
    double *derivatives = new double[num_derivatives];
    for (unsigned int r = 0; r < num_derivatives; r++)
    {
      derivatives[r] = 0.0;
    }// end loop over 'r'
    
    // Declare derivative matrix (of polynomial basis).
    double dmats[1][1] = \
    {{1.0}};
    
    // Declare (auxiliary) derivative matrix (of polynomial basis).
    double dmats_old[1][1] = \
    {{1.0}};
    
    // Loop possible derivatives.
    for (unsigned int r = 0; r < num_derivatives; r++)
    {
      // Resetting dmats values to compute next derivative.
      for (unsigned int t = 0; t < 1; t++)
      {
        for (unsigned int u = 0; u < 1; u++)
        {
          dmats[t][u] = 0.0;
          if (t == u)
          {
          dmats[t][u] = 1.0;
          }
          
        }// end loop over 'u'
      }// end loop over 't'
      
      // Looping derivative order to generate dmats.
      for (unsigned int s = 0; s < n; s++)
      {
        // Updating dmats_old with new values and resetting dmats.
        for (unsigned int t = 0; t < 1; t++)
        {
          for (unsigned int u = 0; u < 1; u++)
          {
            dmats_old[t][u] = dmats[t][u];
            dmats[t][u] = 0.0;
          }// end loop over 'u'
        }// end loop over 't'
        
        // Update dmats using an inner product.
        if (combinations[r][s] == 0)
        {
        for (unsigned int t = 0; t < 1; t++)
        {
          for (unsigned int u = 0; u < 1; u++)
          {
            for (unsigned int tu = 0; tu < 1; tu++)
            {
              dmats[t][u] += dmats0[t][tu]*dmats_old[tu][u];
            }// end loop over 'tu'
          }// end loop over 'u'
        }// end loop over 't'
        }
        
      }// end loop over 's'
      for (unsigned int s = 0; s < 1; s++)
      {
        for (unsigned int t = 0; t < 1; t++)
        {
          derivatives[r] += coefficients0[s]*dmats[s][t]*basisvalues[t];
        }// end loop over 't'
      }// end loop over 's'
    }// end loop over 'r'
    
    // Transform derivatives back to physical element
    for (unsigned int r = 0; r < num_derivatives; r++)
    {
      for (unsigned int s = 0; s < num_derivatives; s++)
      {
        values[r] += transform[r][s]*derivatives[s];
      }// end loop over 's'
    }// end loop over 'r'
    
    // Delete pointer to array of derivatives on FIAT element
    delete [] derivatives;
    
    // Delete pointer to array of combinations of derivatives and transform
    for (unsigned int r = 0; r < num_derivatives; r++)
    {
      delete [] combinations[r];
    }// end loop over 'r'
    delete [] combinations;
    for (unsigned int r = 0; r < num_derivatives; r++)
    {
      delete [] transform[r];
    }// end loop over 'r'
    delete [] transform;
}

/// Evaluate order n derivatives of all basis functions at given point in cell
void ffc_discontinuous_lagrange_0_1d_finite_element_0::evaluate_basis_derivatives_all(unsigned int n,
                                                   double* values,
                                                   const double* coordinates,
                                                   const ufc::cell& c) const
{
    // Element is constant, calling evaluate_basis_derivatives.
    evaluate_basis_derivatives(0, n, values, coordinates, c);
}

/// Evaluate linear functional for dof i on the function f
double ffc_discontinuous_lagrange_0_1d_finite_element_0::evaluate_dof(unsigned int i,
                                   const ufc::function& f,
                                   const ufc::cell& c) const
{
    // Declare variables for result of evaluation.
    double vals[1];
    
    // Declare variable for physical coordinates.
    double y[1];
    const double * const * x = c.coordinates;
    switch (i)
    {
    case 0:
      {
        y[0] = 0.5*x[0][0] + 0.5*x[1][0];
      f.evaluate(vals, y, c);
      return vals[0];
        break;
      }
    }
    
    return 0.0;
}

/// Evaluate linear functionals for all dofs on the function f
void ffc_discontinuous_lagrange_0_1d_finite_element_0::evaluate_dofs(double* values,
                                  const ufc::function& f,
                                  const ufc::cell& c) const
{
    // Declare variables for result of evaluation.
    double vals[1];
    
    // Declare variable for physical coordinates.
    double y[1];
    const double * const * x = c.coordinates;
    y[0] = 0.5*x[0][0] + 0.5*x[1][0];
    f.evaluate(vals, y, c);
    values[0] = vals[0];
}

/// Interpolate vertex values from dof values
void ffc_discontinuous_lagrange_0_1d_finite_element_0::interpolate_vertex_values(double* vertex_values,
                                              const double* dof_values,
                                              const ufc::cell& c) const
{
    // Evaluate function and change variables
    vertex_values[0] = dof_values[0];
    vertex_values[1] = dof_values[0];
}

/// Map coordinate xhat from reference cell to coordinate x in cell
void ffc_discontinuous_lagrange_0_1d_finite_element_0::map_from_reference_cell(double* x,
                                            const double* xhat,
                                            const ufc::cell& c) const
{
    throw std::runtime_error(std::string("map_from_reference_cell not yet implemented (introduced in UFC 2.0)."));
}

/// Map from coordinate x in cell to coordinate xhat in reference cell
void ffc_discontinuous_lagrange_0_1d_finite_element_0::map_to_reference_cell(double* xhat,
                                          const double* x,
                                          const ufc::cell& c) const
{
    throw std::runtime_error(std::string("map_to_reference_cell not yet implemented (introduced in UFC 2.0)."));
}

/// Return the number of sub elements (for a mixed element)
unsigned int ffc_discontinuous_lagrange_0_1d_finite_element_0::num_sub_elements() const
{
    return 0;
}

/// Create a new finite element for sub element i (for a mixed element)
ufc::finite_element* ffc_discontinuous_lagrange_0_1d_finite_element_0::create_sub_element(unsigned int i) const
{
    return 0;
}

/// Create a new class instance
ufc::finite_element* ffc_discontinuous_lagrange_0_1d_finite_element_0::create() const
{
    return new ffc_discontinuous_lagrange_0_1d_finite_element_0();
}


/// Constructor


ffc_discontinuous_lagrange_0_1d_dofmap_0::ffc_discontinuous_lagrange_0_1d_dofmap_0() : ufc::dofmap()
{
    _global_dimension = 0;
}

/// Destructor
ffc_discontinuous_lagrange_0_1d_dofmap_0::~ffc_discontinuous_lagrange_0_1d_dofmap_0()
{
    // Do nothing
}

/// Return a string identifying the dofmap
const char* ffc_discontinuous_lagrange_0_1d_dofmap_0::signature() const
{
    return "FFC dofmap for FiniteElement('Discontinuous Lagrange', Cell('interval', Space(1)), 0, None)";
}

/// Return true iff mesh entities of topological dimension d are needed
bool ffc_discontinuous_lagrange_0_1d_dofmap_0::needs_mesh_entities(unsigned int d) const
{
    switch (d)
    {
    case 0:
      {
        return false;
        break;
      }
    case 1:
      {
        return true;
        break;
      }
    }
    
    return false;
}

/// Initialize dofmap for mesh (return true iff init_cell() is needed)
bool ffc_discontinuous_lagrange_0_1d_dofmap_0::init_mesh(const ufc::mesh& m)
{
    _global_dimension = m.num_entities[1];
    return false;
}

/// Initialize dofmap for given cell
void ffc_discontinuous_lagrange_0_1d_dofmap_0::init_cell(const ufc::mesh& m,
                              const ufc::cell& c)
{
    // Do nothing
}

/// Finish initialization of dofmap for cells
void ffc_discontinuous_lagrange_0_1d_dofmap_0::init_cell_finalize()
{
    // Do nothing
}

/// Return the topological dimension of the associated cell shape
unsigned int ffc_discontinuous_lagrange_0_1d_dofmap_0::topological_dimension() const
{
    return 1;
}

/// Return the geometric dimension of the associated cell shape
unsigned int ffc_discontinuous_lagrange_0_1d_dofmap_0::geometric_dimension() const
{
    return 1;
}

/// Return the dimension of the global finite element function space
unsigned int ffc_discontinuous_lagrange_0_1d_dofmap_0::global_dimension() const
{
    return _global_dimension;
}

#ifndef UFC_BACKWARD_COMPATIBILITY
/// Return the dimension of the local finite element function space for a cell
unsigned int ffc_discontinuous_lagrange_0_1d_dofmap_0::local_dimension(const ufc::cell& c) const
{
    return 1;
}

/// Return the maximum dimension of the local finite element function space
unsigned int ffc_discontinuous_lagrange_0_1d_dofmap_0::max_local_dimension() const
{
    return 1;
}
#else
/// Return the dimension of the local finite element function space for a cell
unsigned int ffc_discontinuous_lagrange_0_1d_dofmap_0::local_dimension() const
{
    return 1;
}
#endif

/// Return the number of dofs on each cell facet
unsigned int ffc_discontinuous_lagrange_0_1d_dofmap_0::num_facet_dofs() const
{
    return 0;
}

/// Return the number of dofs associated with each cell entity of dimension d
unsigned int ffc_discontinuous_lagrange_0_1d_dofmap_0::num_entity_dofs(unsigned int d) const
{
    switch (d)
    {
    case 0:
      {
        return 0;
        break;
      }
    case 1:
      {
        return 1;
        break;
      }
    }
    
    return 0;
}

/// Tabulate the local-to-global mapping of dofs on a cell
void ffc_discontinuous_lagrange_0_1d_dofmap_0::tabulate_dofs(unsigned int* dofs,
                                  const ufc::mesh& m,
                                  const ufc::cell& c) const
{
    dofs[0] = c.entity_indices[1][0];
}

/// Tabulate the local-to-local mapping from facet dofs to cell dofs
void ffc_discontinuous_lagrange_0_1d_dofmap_0::tabulate_facet_dofs(unsigned int* dofs,
                                        unsigned int facet) const
{
    switch (facet)
    {
    case 0:
      {
        
        break;
      }
    case 1:
      {
        
        break;
      }
    }
    
}

/// Tabulate the local-to-local mapping of dofs on entity (d, i)
void ffc_discontinuous_lagrange_0_1d_dofmap_0::tabulate_entity_dofs(unsigned int* dofs,
                                  unsigned int d, unsigned int i) const
{
    if (d > 1)
    {
    throw std::runtime_error(std::string("d is larger than dimension (1)"));
    }
    
    switch (d)
    {
    case 0:
      {
        
        break;
      }
    case 1:
      {
        if (i > 0)
      {
      throw std::runtime_error(std::string("i is larger than number of entities (0)"));
      }
      
      dofs[0] = 0;
        break;
      }
    }
    
}

/// Tabulate the coordinates of all dofs on a cell
void ffc_discontinuous_lagrange_0_1d_dofmap_0::tabulate_coordinates(double** coordinates,
                                         const ufc::cell& c) const
{
    const double * const * x = c.coordinates;
    
    coordinates[0][0] = 0.5*x[0][0] + 0.5*x[1][0];
}

/// Return the number of sub dofmaps (for a mixed element)
unsigned int ffc_discontinuous_lagrange_0_1d_dofmap_0::num_sub_dofmaps() const
{
    return 0;
}

/// Create a new dofmap for sub dofmap i (for a mixed element)
ufc::dofmap* ffc_discontinuous_lagrange_0_1d_dofmap_0::create_sub_dofmap(unsigned int i) const
{
    return 0;
}

/// Create a new class instance
ufc::dofmap* ffc_discontinuous_lagrange_0_1d_dofmap_0::create() const
{
    return new ffc_discontinuous_lagrange_0_1d_dofmap_0();
}


