// Copyright (C) 2006 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// Modified by Garth N. Wells, 2006.
// Modified by Aurélien Larcher, 2014.
//
// First added:  2006-06-12
// Last changed: 2014-06-12

#include <dolfin/mesh/Point.h>

#include <cmath>

namespace dolfin
{

} /* namespace dolfin */
