// Copyright (C) 2013-2014 Aurélien Larcher
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2013-06-11
// Last changed: 2014-02-04

#include <dolfin/fem/DofMapCache.h>
#include <dolfin/fem/DofMap.h>
#include <dolfin/fem/Form.h>
#include <dolfin/function/Function.h>

#include <iomanip>

#include <ufc.h>

namespace dolfin
{

//-----------------------------------------------------------------------------
DofMapCache::DofMapCache()
{
  cache_.clear();
  rlist_.clear();
}

//-----------------------------------------------------------------------------
DofMapCache::~DofMapCache()
{
  if (cache_.size() != 0)
  {
    disp();
    error("DofMapCache is not empty: "
          "some dof maps have not been properly released");
  }
  for (container_t::iterator it = cache_.begin(); it != cache_.end(); ++it)
  {
    delete it->second.item;
  }
  cache_.clear();
}

//-----------------------------------------------------------------------------
DofMap& DofMapCache::acquire(Mesh& mesh, Form const& form, uint const& i)
{
  // Update dof maps of form:
  // This triggers creation of a dof map set if needed and acquistion of a token
  // for each coefficient of the form.
  // If the dof map set has been created the call just return without doing
  // anything.
  form.update_dofmaps();

  // Create UFC dof map for the i-th coefficient to get the signature
  ufc::dofmap * ufc_dofmap = form.create_dofmap(i);

  // Do not transfer ownership to the possibly created dofmap
  // The UFC dofmap is cloned as member attribute of the DOLFIN dofmap
  DofMap * ret = &acquire(mesh, *ufc_dofmap, false);

  // Delete UFC dof map as it is not longer used
  delete ufc_dofmap;

  return *ret;
}

//-----------------------------------------------------------------------------
DofMap& DofMapCache::acquire(Mesh& mesh, ufc::dofmap& dofmap, bool owner)
{
  DofMap * ret = NULL;
  std::string const h = DofMap::make_hash(mesh, dofmap);
  container_t::iterator it = cache_.find(h);

  if (it == cache_.end())
  {
    // Create DOLFIN dofmap and insert in map
    ret = new DofMap(mesh, dofmap, owner);
    cache_.insert(item_t(h, token_t(ret)));

    if (rlist_.find(ret) == rlist_.end())
    {
      rlist_.insert(ritem_t(ret, h));
    }
    else
    {
      error("DofMap pointer has already been registered with another hash");
    }
  }
  else
  {
    // The dofmap has already been created
    ret = it->second.item;
    dolfin_assert(ret);

    // Check hash consistency
    std::string const dm_h = ret->hash();
    if (h != dm_h)
    {
      disp();
      std::stringstream ss;
      ss << std::endl << "DofMap@" << ret << std::endl
         << "Signature to be inserted  : " << h << std::endl
         << "Signature of DofMap       : " << dm_h << std::endl
         << "DofMap object refers to two different signatures";
      error(ss.str());
    }
    it->second.count++;

    // Ownership was transfered but the dofmap already exists, the UFC dofmap
    // passed as argument will not be used.
    if (owner)
    {
      delete &dofmap;
    }
  }
  return *ret;
}

//-----------------------------------------------------------------------------
void DofMapCache::release(DofMap& dofmap)
{
  std::string const h = dofmap.hash();
  rlist_t::iterator it = rlist_.find(&dofmap);
  std::string const expected_h = it->second;

  if (it == rlist_.end())
  {
    error("Trying to release inexistent DofMap");
  }
  else
  {
    if (h != expected_h)
    {
      disp();
      std::stringstream ss;
      ss << std::endl << "DofMap@" << &dofmap << std::endl
         << "Signature to be released     : " << h << std::endl
         << "Signature already registered : " << expected_h << std::endl
         << "DofMap object refers to two different signatures";
      error(ss.str());
    }
    container_t::iterator dm_it = cache_.find(expected_h);
    token_t& dm_token = dm_it->second;
    dm_token.count--;
    //message("Release dofmap: %s",h.c_str());
    if (dm_token.count == 0)
    {
      delete dm_token.item;
      rlist_.erase(it);
      cache_.erase(dm_it);
    }
  }
}

//-----------------------------------------------------------------------------
void DofMapCache::disp() const
{
  message("Number of DofMaps in cache : %i", cache_.size());
  message("Cache :");
  for (container_t::const_iterator it = cache_.begin(); it != cache_.end();
      ++it)
  {
    std::cout << std::setw(128) << it->first << " : @" << it->second.item
              << " : " << it->second.count << std::endl;
  }
  message("Reverse list :");
  for (rlist_t::const_iterator it = rlist_.begin(); it != rlist_.end(); ++it)
  {
    std::cout << "@" << it->first << " : " << it->second << std::endl;
  }
}

}

