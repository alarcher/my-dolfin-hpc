\chapter{Linear algebra}


\dolfin{} provides a high-performance linear algebra library,
including matrices and vectors, a set of linear solvers,
and preconditioners. The core part of the
functionality is provided through wrappers that provide a common
interface to the functionality of the linear algebra libraries
PETSc~\cite{www:petsc} and JANPACK~\cite{www:janpack}.

%------------------------------------------------------------------------------
\section{Matrices and vectors}
\index{\texttt{Matrix}}
\index{\texttt{Vector}}

The two basic linear algebra data structures are the classes
\texttt{Matrix} and \texttt{Vector}, representing a sparse $M\times
N$ matrix and a vector of length $N$ respectively. 

The following code demonstrates how to create a matrix and a vector:
\begin{code}
Matrix A(M, N);
Vector x(N);
\end{code}
Alternatively, the matrix and the vector may be created by
\begin{code}
Matrix A;
Vector x;

A.init(M, N);
x.init(N);
\end{code}

The following code demonstrates how to access the size and the
elements of a matrix and a vector:
\begin{code}
A(5, 5) = 1.0;
real a = A(4, 3);

x(3) = 2.0;
real b = x(5);

unsigned int M = A.size(0);
unsigned int N = A.size(1);

N = x.size();
\end{code}

In addition, \dolfin{} provides optimized functions for setting the
values of a set of entries in a matrix or vector:
\begin{code}
real block[] = {2, 4, 6};
int rows[] = {0, 1, 2};
int cols[] = {0, 1, 2};
  
A.set(block, rows, cols, 3);
\end{code}
Alternatively, the set of values given by the array \texttt{block} can
be added to the entries given by the arrays \texttt{rows} and
\texttt{cols}:
\begin{code}
real block[] = {2, 4, 6};
int rows[] = {0, 1, 2};
int cols[] = {0, 1, 2};
  
A.add(block, rows, cols, 3);
\end{code}
These functions are particularly useful for efficient assembly of a (sparse)
matrix from a bilinear form.

\subsection{Sparse matrices}
\index{sparse matrix}

The default \dolfin{} class \texttt{Matrix} is a sparse matrix, which
efficiently represents the discretization of a partial differential
equation where most entries in the matrix are zero. 

If \dolfin{} has been compiled with support for PETSc, then the sparse
matrix is represented as a sparse PETSc matrix Alternatively, the
class \texttt{PETScMatrix} may be used, together with the
corresponding class \texttt{PETScVector}.

If \dolfin{} has been compiled without support for PETSc, then the
sparse matrix is represented as a JANPACK matrix. Alternatively,
the class \texttt{JANPACKMatrix} may be used, together with the
corresponding class \texttt{JANPACKVector}.

\subsection{The common interface}
\index{\texttt{GenericMatrix}}
\index{\texttt{GenericVector}}

Although \dolfin{} differentiates between sparse and dense data
structures, the two classes \texttt{GenericMatrix} and
\texttt{GenericVector} define a common interface to all matrices and
vectors. Refer to the \emph{DOLFIN programmer's reference} for the
exact specification of these interfaces.

The following points should be noted:
\begin{enumerate}
\item
  If you want a specific backend like PETSc, then use
  \texttt{PETScVector}/\texttt{PETScMatrix}.
\item
  If you don't care about the backend, then use \texttt{Vector}/\texttt{Matrix}.
\item
  The backend for \texttt{Vector}/\texttt{Matrix} is determined by the
  parameter \texttt{linear algebra backend}, which can be changed
  during runtime as follows:
\begin{code}
dolfin_set("linear algebra backend", "JANPACK");
\end{code}
For more information on the parameter system see chapter \ref{sec:parameters}.
\item
  If you write a \emph{function} that should be able to accept vectors
  and matrices from any backend as input, then use
  \texttt{GenericVector}/\texttt{GenericMatrix}.
\end{enumerate}


%------------------------------------------------------------------------------
\section{Solving linear systems}
\index{linear systems}

\dolfin{} provides a set of efficient solvers for linear systems of
the form
\begin{equation}
  Ax = b,
\end{equation}
where $A$ is an $N\times N$ matrix and where $x$ and $b$ are vectors
of length $N$. Both iterative (Krylov subspace) solvers Multilevel (algebraic multigrid) solvers and direct
(LU) solvers are provided.

\subsection{Iterative methods}
\index{iterative methods}
\index{\texttt{GMRES}}
\index{GMRES method}
\index{\texttt{KrylovSolver}}
\index{Krylov subspace methods}

A linear system may be solved by the GMRES Krylov method as follows:
\begin{code}
Matrix A;
Vector x, b:

KrylovSolver solver(gmres, ilu);
solver.solve(A, x, b);
\end{code}
The object \texttt{KrylovSolverolver} can be used for repeated solution of the same linear system and alos allow sthe specification of both Krylov method and the preconditioner.

\subsubsection{Krylov methods}
\index{conjugate gradient method}
\index{BiCGStab}

\dolfin{} provides the following set of Krylov methods:

\begin{center}
\begin{tabular}{|l|l|}
\hline
\texttt{cg}              & The conjugate gradient method \\
\hline
\texttt{gmres}           & The GMRES method \\
\hline
\texttt{bicgstab}        & The stabilized biconjugate gradient squared
method \\
\hline
\texttt{default\_method} & Default choice of Krylov method \\
\hline
\end{tabular}
\end{center}

\subsubsection{Preconditioners}
\index{preconditioners}
\index{ILU}
\index{incomplete LU factorization}
\index{SOR}
\index{successive over-relaxation}
\index{Jacobi}
\index{AMG}
\index{algebraic multigrid}

\dolfin{} provides the following set of preconditioners:

\begin{center}
\begin{tabular}{|l|l|}
\hline
\texttt{none}        & No preconditioning \\
\hline
\texttt{jacobi}      & Simple Jacobi preconditioning \\
\hline
\texttt{sor}         & SOR, successive over-relaxation \\
\hline
\texttt{ilu}         & Incomplete LU factorization \\
\hline
\texttt{dilu}        & diagonal Incomplete LU factorization \\
\hline
\texttt{icc}         & Incomplete Cholesky factorization \\
\hline
\texttt{amg}         & Algebraic multigrid  \\
\hline
\texttt{default\_pc} & Default choice of preconditioner \\
\hline
\end{tabular}
\end{center}

\subsection{Multilevel methods}
\index{multilevel methods}
\index{AMG}
\index{\texttt{AMGSolver}}
A linear system may be solved by a W-cycle Algebraic multigrid as follows:
\begin{code}
Matrix A;
Vector x, b:

AMGSolver solver(w, mg_gauss_seidel, rs);
solver.solve(A, x, b);
\end{code}

\subsubsection{Multigrid schemes}
\dolfin{} provides the following set of multigrid schemes:
\begin{center}
\begin{tabular}{|l|l|}
\hline
\texttt{mgv}        & V-cycle scheme\\
\hline
\texttt{mgw}      & W-cycle scheme\\
\hline
\texttt{fmv}       & Full Multigrid V-cycle\\
\hline
\end{tabular}
\end{center}
\subsubsection{Multigrid smoothers}
\dolfin{} provides the following set of multigrid smoothers:
\begin{center}
\begin{tabular}{|l|l|}
\hline
\texttt{rs}        & Classic Rugen-St\"{u}ben \\
\hline
\texttt{cljp}      &  Cleary-Luby-Jones-Plassman \\
\hline
\texttt{pmis}       & Parallel Modified Independent Set\\
\hline
\texttt{hmis}       &Hybrid Modified Independent Set\\
\hline
\end{tabular}
\end{center}
\subsubsection{Coarsening schemes}
\dolfin{} provides the following set of coarsening strategies:
\begin{center}
\begin{tabular}{|l|l|}
\hline
\texttt{mg\_jacobi}        & Jacobi \\
\hline
\texttt{mg\_gauss\_seidel}      & Gauss-Seidel\\
\hline
\texttt{mg\_sor}       & Successive Over Relaxation\\
\hline
\end{tabular}
\end{center}
\subsection{Direct methods}
\index{\texttt{LU}}
\index{\texttt{LUSolver}}
\index{direct methods}
\index{LU factorization}

A linear system may be solved by a direct LU factorization as follows:
\begin{code}
Matrix A;
Vector x, b;

LUSolver solver;
solver.solve(A, x, b);
\end{code}
The object \texttt{LUsolver} can be used for repeated solution of the same linear system.

%------------------------------------------------------------------------------
\section{Linear algebra backends}
\index{linear algebra backends}

\subsection{PETSc}
\index{PETSc}
%
PETSc is a suite of data structures and routines for the scalable
(parallel) solution of scientific applications modeled by partial
differential equations. It employs the MPI standard for all 
message-passing communication.

\dolfin{} wrappers for PETSc linear algebra is provided through the
classes \texttt{PETScMatrix} and \texttt{PETScVector}. Direct access
to the PETSc data structures is available through the member functions
\texttt{mat()} and \texttt{vec()}, which return the PETSc \texttt{Mat}
and \texttt{Vec} pointers respectively. For advanced usage not covered
by the common \dolfin{} interface specified by the classes
\texttt{GenericMatrix} and \texttt{GenericVector}, refer directly to
the documentation of PETSc.
%------------------------------------------------------------------------------
\subsection{JANPACK}
\index{JANPACK}

JANPACK is a suite of data structures, Krylov and Multilevel (AMG)
solvers for solving large linear systems. The library uses an
alternative sparse matrix representation, optimized for inserting or
modifying entries at a low cost.

\dolfin{} wrappers for JANPACK is provided through the classes
\texttt{JANPACKMatrix} and \texttt{JANPACKVector}. Direct access to
the data structures is available through the member functions
\texttt{mat()} and \texttt{vec()}, which return a \verb|jp_mat_t|
and \verb|jp_vec_t| pointers respectively. For advanced usage not
covered by the common \dolfin{} interface specified by the classes
\texttt{GenericMatrix} and \texttt{GenericVector}, refer directly to
the documentation for JANPACK.
%------------------------------------------------------------------------------
