#ifndef __DOLFIN_UFL_H
#define __DOLFIN_UFL_H

// DOLFIN ufl interface

#include <dolfin/ufl/UFLarray.h>
#include <dolfin/ufl/UFLdict.h>
#include <dolfin/ufl/UFLrepr.h>
#include <dolfin/ufl/UFL_tuple.h>
#include <dolfin/ufl/UFLtype.h>
#include <dolfin/ufl/UFLAlgebra.h>
#include <dolfin/ufl/UFLArgument.h>
#include <dolfin/ufl/UFLCell.h>
#include <dolfin/ufl/UFLCellSurfaceArea.h>
#include <dolfin/ufl/UFLCellVolume.h>
#include <dolfin/ufl/UFLCircumradius.h>
#include <dolfin/ufl/UFLClass.h>
#include <dolfin/ufl/UFLCoefficient.h>
#include <dolfin/ufl/UFLConditional.h>
#include <dolfin/ufl/UFLData.h>
#include <dolfin/ufl/UFLDifferentiation.h>
#include <dolfin/ufl/UFLDomain.h>
#include <dolfin/ufl/UFLElementList.h>
#include <dolfin/ufl/UFLEnrichedElement.h>
#include <dolfin/ufl/UFLEquation.h>
#include <dolfin/ufl/UFLExpression.h>
#include <dolfin/ufl/UFLFacetArea.h>
#include <dolfin/ufl/UFLFacetNormal.h>
#include <dolfin/ufl/UFLFamily.h>
#include <dolfin/ufl/UFLFiniteElement.h>
#include <dolfin/ufl/UFLFiniteElementSpace.h>
#include <dolfin/ufl/UFLForm.h>
#include <dolfin/ufl/UFLFormData.h>
#include <dolfin/ufl/UFLGeometricQuantity.h>
#include <dolfin/ufl/UFLIndex.h>
#include <dolfin/ufl/UFLIndexed.h>
#include <dolfin/ufl/UFLIndexSum.h>
#include <dolfin/ufl/UFLIntegral.h>
#include <dolfin/ufl/UFLList.h>
#include <dolfin/ufl/UFLMixedElement.h>
#include <dolfin/ufl/UFLObject.h>
#include <dolfin/ufl/UFLQuadratureScheme.h>
#include <dolfin/ufl/UFLRestrictedElement.h>
#include <dolfin/ufl/UFLSpace.h>
#include <dolfin/ufl/UFLSpatialCoordinate.h>
#include <dolfin/ufl/UFLTensorElement.h>
#include <dolfin/ufl/UFLTensors.h>
#include <dolfin/ufl/UFLTuple.h>
#include <dolfin/ufl/UFLValueArray.h>
#include <dolfin/ufl/UFLVariable.h>
#include <dolfin/ufl/UFLVectorElement.h>

#endif
