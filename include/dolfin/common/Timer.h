// Copyright (C) 2008 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2008-06-13
// Last changed: 2008-06-13

#ifndef __DOLFIN_TIMER_H
#define __DOLFIN_TIMER_H

#include <dolfin/common/timing.h>

namespace dolfin
{

  /// A timer can be used for timing tasks. The basic usage is
  ///
  ///   Timer timer("Assembling over cells");
  ///
  /// The timer is started at construction and timing ends
  /// when the timer is destroyed (goes out of scope). It is
  /// also possible to start and stop a timer explicitly by
  ///
  ///   timer.start();
  ///   timer.stop();

  class Timer
  {
  public:

    /// Create timer
    Timer(std::string task) : task(task), t(time()), stopped(false) {}

    /// Destructor
    ~Timer() { if (!stopped) stop(); }

    /// Start timer
    inline void start() { t = time(); stopped = false; }

    /// Stop timer
    inline void stop() { timing(task.c_str(), time() - t); stopped = true; }

  private:

    // Name of task
    std::string task;

    // Start time
    real t;

    // True if timer has been stopped
    bool stopped;

  };

}

#endif
