#ifndef __DOLFIN_HEADER_COMMON_H
#define __DOLFIN_HEADER_COMMON_H

// DOLFIN common classes

#include <dolfin/common/constants.h>
#include <dolfin/common/byteswap.h>
#include <dolfin/common/system.h>
#include <dolfin/common/timing.h>
#include <dolfin/common/types.h>
#include <dolfin/common/Array.h>
#include <dolfin/common/List.h>
#include <dolfin/common/Test.h>
#include <dolfin/common/Timer.h>
#include <dolfin/common/Variable.h>

#endif /* __DOLFIN_HEADER_COMMON_H */
