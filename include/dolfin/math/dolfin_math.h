#ifndef __DOLFIN_HEADER_MATH_H
#define __DOLFIN_HEADER_MATH_H

// DOLFIN math

#include <dolfin/math/basic.h>
#include <dolfin/math/Lagrange.h>
#include <dolfin/math/Legendre.h>

#endif /* __DOLFIN_HEADER_MATH_H */
