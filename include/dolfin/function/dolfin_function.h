#ifndef __DOLFIN_HEADER_FUNCTION_H
#define __DOLFIN_HEADER_FUNCTION_H

// DOLFIN function interface

#include <dolfin/function/Analytic.h>
#include <dolfin/function/Constant.h>
#include <dolfin/function/Expression.h>
#include <dolfin/function/Field.h>
#include <dolfin/function/Function.h>
#include <dolfin/function/FunctionDecomposition.h>
#include <dolfin/function/FunctionInterpolation.h>
#include <dolfin/function/Real.h>
#include <dolfin/function/UFCFunction.h>
#include <dolfin/function/Value.h>


#endif /* __DOLFIN_HEADER_FUNCTION_H */
