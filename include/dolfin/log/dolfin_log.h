#ifndef __DOLFIN_HEADER_LOG_H
#define __DOLFIN_HEADER_LOG_H

/// DOLFIN log interface

#include <dolfin/log/log.h>
#include <dolfin/log/LogStream.h>

#endif /* __DOLFIN_HEADER_LOG_H */
