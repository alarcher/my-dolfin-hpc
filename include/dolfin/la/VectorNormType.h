// Copyright (C) 2007 Garth N. Wells.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2007-07-03
// Last changed:

#ifndef __DOLFIN_VECTOR_NORM_TYPE_H
#define __DOLFIN_VECTOR_NORM_TYPE_H

namespace dolfin
{

  // List of predefined norm types

  enum VectorNormType {l1, l2, linf};

}

#endif
