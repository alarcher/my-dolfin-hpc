// Copyright (C) 2017 Aurelien Larcher.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:   2017-11-01
// Last changed:  2017-11-01

#ifndef __DOLFIN_CELL_TYPES_H
#define __DOLFIN_CELL_TYPES_H

#include <dolfin/mesh/PointCell.h>
#include <dolfin/mesh/IntervalCell.h>
#include <dolfin/mesh/TriangleCell.h>
#include <dolfin/mesh/TetrahedronCell.h>
#include <dolfin/mesh/QuadrilateralCell.h>
#include <dolfin/mesh/HexahedronCell.h>

namespace dolfin
{

} /* namespace dolfin */

#endif /* __DOLFIN_CELL_TYPES_H */
